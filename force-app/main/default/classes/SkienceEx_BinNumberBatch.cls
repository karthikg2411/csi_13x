/*
* @author         Skience
* @Class Name	  SkienceEx_BinNumberBatch
* @modifiedBy     Karthik
* @maintainedBy   Karthik Govindaraju   
* @version        1.0
* @created        2021-Mar-05
* @modified       2021-Mar-05
* @changes
* v1.1 - BIN Number linkage 
*/
global class SkienceEx_BinNumberBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    global Database.Querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([Select Id, BIN_Number__c,SkienceEx_Bin_Related_FA__c  From FinServ__FinancialAccount__c where BIN_Number__c != null And SkienceEx_Bin_Related_FA__c = null ]);
    }
    
    global void Execute(Database.BatchableContext BC, List<FinServ__FinancialAccount__c> scope){
        set<String> binnumberSet= new set <String>();
        map<String,String> binNumFaIdMap = new map<String,String>();
        List<FinServ__FinancialAccount__c> updateFinAccounts = new List<FinServ__FinancialAccount__c>();
        if( scope != null && scope.size() > 0 ){
            //Identify unique bin number
            for( FinServ__FinancialAccount__c finAccRec : scope ){
                binnumberSet.add(finAccRec.BIN_Number__c);
            }
            
            if( binnumberSet != null && binnumberSet.size() > 0 ){
                //Fetch FA record related to binnumber
                List<FinServ__FinancialAccount__c> finAccRecLst = [ Select Id, FinServ__FinancialAccountNumber__c From FinServ__FinancialAccount__c Where FinServ__FinancialAccountNumber__c IN :binnumberSet  ];
                for( FinServ__FinancialAccount__c finAccRec : finAccRecLst ){
                    binNumFaIdMap.put(finAccRec.FinServ__FinancialAccountNumber__c, finAccRec.Id );
                }
                
                if( binNumFaIdMap != null && binNumFaIdMap.size() > 0 ){
                    for( FinServ__FinancialAccount__c finAccRec : scope ){
                        if( binNumFaIdMap.containsKey( finAccRec.BIN_Number__c ) ){
                            finAccRec.SkienceEx_Bin_Related_FA__c = binNumFaIdMap.get(finAccRec.BIN_Number__c);
                            updateFinAccounts.add(finAccRec);
                        }
                    }
                }
                
                if( updateFinAccounts != null && updateFinAccounts.size() > 0 ){
                    Database.SaveResult[] results = database.update(updateFinAccounts, false);
                    //To do later stages for error handling
                }
            }
        }        
    }
    
    global void finish(Database.BatchableContext BC){
        //No action required
    }
    
}