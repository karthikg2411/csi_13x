/*
* @author         Skience
* @Class Name     SkienceEx_BinNumberScheduler
* @modifiedBy     Karthik
* @maintainedBy   Karthik Govindaraju   
* @version        1.0
* @created        2021-Mar-05
* @modified       2021-Mar-05
* @changes
* v1.1 - BIN Number linkage - Scheduler
*/
global class SkienceEx_BinNumberScheduler implements Schedulable {
    
    global static String sched = '0 00 00 * * ?';  //Every Day at Midnight 
    global static string scheName;
    
    global static String scheduleMe() {
        if(Test.isRunningTest()){
            scheName = 'BinNumberBatchTest';
        }else{
            scheName = 'BinNumberBatch';
        }
        SkienceEx_BinNumberScheduler SC = new SkienceEx_BinNumberScheduler(); 
        return System.schedule(scheName, sched, SC);
    }
    
    global void execute(SchedulableContext sc) {

        SkienceEx_BinNumberBatch binBatch = new SkienceEx_BinNumberBatch();
        ID batchprocessid = Database.executeBatch(binBatch,7000);           
    }

}