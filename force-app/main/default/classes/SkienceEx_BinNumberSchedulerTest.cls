/*
* @author         Skience
* @Class Name     SkienceEx_BinNumberSchedulerTest
* @modifiedBy     Karthik
* @maintainedBy   Karthik Govindaraju   
* @version        1.0
* @created        2021-Mar-05
* @modified       2021-Mar-05
* @changes
* v1.1 - BIN Number linkage - Test class
*/
@isTest(seeAllData=false)
public class SkienceEx_BinNumberSchedulerTest {
    
    public static testmethod void testBinNumber(){
        User runAsUsr = [Select Id from User where Profile.Name = 'System Administrator' and IsActive = True AND Supervisor__c != Null Limit 1];
        System.runAs(runAsUsr){
            Id premierProfId = [Select Id From profile Where Name = 'Premier Advisor' Limit 1].Id;
            Id faProfId = [Select Id From profile Where Name = 'Financial Advisor' Limit 1].Id;
            
            Id paRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            Id entityRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Entity').getRecordTypeId();
            Id preRecTypeId = Schema.SObjectType.FinServ__FinancialAccount__c.getRecordTypeInfosByDeveloperName().get('BankingAccount').getRecordTypeId();
            Id cisRecTypeId = Schema.SObjectType.FinServ__FinancialAccount__c.getRecordTypeInfosByDeveloperName().get('InvestmentAccount').getRecordTypeId();
            
            List<User> userList = new List<User>();
            
            User userObj1 = new User(FirstName = 'FA',
                                     LastName = 'Advisor1',
                                     Email = 'faadvisor1@testemail.com',
                                     Title = 'Financial Advisor I',
                                     Line_of_business__c = 'CIS',
                                     ProfileId = faProfId,
                                     UserName = 'faadvisor1@testemail.com',
                                     Alias = 'faadv1',
                                     isActive = True,
                                     EmailEncodingKey = 'ISO-8859-1',
                                     LanguageLocaleKey = 'en_US',
                                     LocaleSidKey = 'en_US',
                                     TimeZoneSidKey = 'America/New_York',
                                     Supervisor__c = runAsUsr.Id);
            userList.add(userObj1);
            
            User userObj2 = new User(FirstName = 'FA',
                                     LastName = 'Advisor2',
                                     Email = 'faadvisor2@testemail.com',
                                     Title = 'Financial Advisor II',
                                     Line_of_business__c = 'CIS',
                                     ProfileId = faProfId,
                                     UserName = 'faadvisor2@testemail.com',
                                     Alias = 'faadv2',
                                     isActive = True,
                                     EmailEncodingKey = 'ISO-8859-1',
                                     LanguageLocaleKey = 'en_US',
                                     LocaleSidKey = 'en_US',
                                     TimeZoneSidKey = 'America/New_York',
                                     Supervisor__c = runAsUsr.Id);
            userList.add(userObj2);
            
            User userObj3 = new User(FirstName = 'FA',
                                     LastName = 'Advisor3',
                                     Email = 'faadvisor3@testemail.com',
                                     Title = 'Financial Advisor III',
                                     Line_of_business__c = 'CIS',
                                     ProfileId = faProfId,
                                     UserName = 'faadvisor3@testemail.com',
                                     Alias = 'faadv3',
                                     isActive = True,
                                     EmailEncodingKey = 'ISO-8859-1',
                                     LanguageLocaleKey = 'en_US',
                                     LocaleSidKey = 'en_US',
                                     TimeZoneSidKey = 'America/New_York',
                                     Supervisor__c = runAsUsr.Id);
            userList.add(userObj3);
            
            User userObj4 = new User(FirstName = 'Premier',
                                     LastName = 'Advisor1',
                                     Email = 'premieradvisor1@testemail.com',
                                     Title = 'Premier Advisor',
                                     Line_of_business__c = 'Premier',
                                     ProfileId = premierProfId,
                                     UserName = 'premieradvisor1@testemail.com',
                                     Alias = 'preadv1',
                                     isActive = True,
                                     EmailEncodingKey = 'ISO-8859-1',
                                     LanguageLocaleKey = 'en_US',
                                     LocaleSidKey = 'en_US',
                                     TimeZoneSidKey = 'America/New_York',
                                     Supervisor__c = runAsUsr.Id);
            userList.add(userObj4);
            
            User userObj5 = new User(FirstName = 'Premier',
                                     LastName = 'Advisor2',
                                     Email = 'premieradvisor2@testemail.com',
                                     Title = 'Premier Advisor',
                                     Line_of_business__c = 'Premier',
                                     ProfileId = premierProfId,
                                     UserName = 'premieradvisor2@testemail.com',
                                     Alias = 'preadv2',
                                     isActive = True,
                                     EmailEncodingKey = 'ISO-8859-1',
                                     LanguageLocaleKey = 'en_US',
                                     LocaleSidKey = 'en_US',
                                     TimeZoneSidKey = 'America/New_York',
                                     Supervisor__c = runAsUsr.Id);
            userList.add(userObj5);
            
            TriggerRunHelper.byPassUserTrgRun();
            insert userList;
            
            List<Account> accList = new List<Account>();
            
            Account accObj1 = new Account(FirstName = 'Person',
                                          LastName = 'Account1',
                                          OwnerId = runAsUsr.Id,
                                          Date_of_Birth__pc = System.today()-500,
                                          Phone = '1234567890',
                                          AccountNumber = '123456',
                                          BillingStreet = '3022 Strother Street',
                                          BillingCity = 'Into',
                                          BillingState = 'AL',
                                          BillingPostalCode = '43222',
                                          BillingCountry = 'United States',
                                          RecordTypeId = paRecTypeId,
                                          TIN__c = '789678967');
            accList.add(accObj1);
            
            Account accObj2 = new Account(FirstName = 'Person',
                                          LastName = 'Account2',
                                          OwnerId = runAsUsr.Id,
                                          Date_of_Birth__pc = System.today()-700,
                                          AccountNumber = '123454',
                                          Phone = '0987654321',
                                          BillingStreet = '3022 Strother Street',
                                          BillingCity = 'Into',
                                          BillingState = 'AL',
                                          BillingPostalCode = '43222',
                                          BillingCountry = 'United States',
                                          RecordTypeId = paRecTypeId,
                                          TIN__c = '789678968');
            accList.add(accObj2);
            
            Account accObj3 = new Account(Name = 'Entity1',
                                          OwnerId = runAsUsr.Id,
                                          AccountNumber = '123455',
                                          RecordTypeId = entityRecTypeId,
                                          TIN__c = '789678969',
                                          BillingStreet = '3022 Strother Street',
                                          BillingCity = 'Into',
                                          BillingState = 'AL',
                                          BillingPostalCode = '43222',
                                          BillingCountry = 'United States',
                                          Phone = '2345674893');
            accList.add(accObj3);
            
            /* V1.1 Starts */
            
            Account accObj4 = new Account(Name = 'Entity2',
                                          OwnerId = runAsUsr.Id,
                                          AccountNumber = '123450',
                                          RecordTypeId = entityRecTypeId,
                                          TIN__c = '789678961',
                                          BillingStreet = '3022 Strother Street',
                                          BillingCity = 'Into',
                                          BillingState = 'AL',
                                          BillingPostalCode = '43222',
                                          BillingCountry = 'United States',
                                          Phone = '2345674813');
            accList.add(accObj4);
            
            /* V1.1 Ends */
            
            insert accList;
            
            List<FinServ__FinancialAccount__c> finAccList = new List<FinServ__FinancialAccount__c>();
            
            FinServ__FinancialAccount__c finAccObj1 = new FinServ__FinancialAccount__c(Name = 'Premier Account 1',
                                                                                       FinServ__Ownership__c = 'Joint',
                                                                                       FinServ__PrimaryOwner__c = accObj1.Id,
                                                                                       FinServ__JointOwner__c = accObj2.Id,
                                                                                       FinServ__Status__c = 'Open',
                                                                                       FinServ__OpenDate__c = System.today()-50,
                                                                                       RecordTypeId = preRecTypeId,
                                                                                       OwnerId = userObj4.Id,
                                                                                       FinServ__FinancialAccountNumber__c = '123'
                                                                                      );
            finAccList.add(finAccObj1);
            
            FinServ__FinancialAccount__c finAccObj2 = new FinServ__FinancialAccount__c(Name = 'Premier Account 2',
                                                                                       FinServ__Ownership__c = 'Individual',
                                                                                       FinServ__PrimaryOwner__c = accObj1.Id,
                                                                                       FinServ__Status__c = 'Open',
                                                                                       FinServ__OpenDate__c = System.today()-40,
                                                                                       RecordTypeId = preRecTypeId,
                                                                                       OwnerId = userObj5.Id,
                                                                                       FinServ__FinancialAccountNumber__c = '345',
                                                                                       BIN_Number__c = '123'
                                                                                      );
            finAccList.add(finAccObj2);
            
            FinServ__FinancialAccount__c finAccObj3 = new FinServ__FinancialAccount__c(Name = 'Premier Account 3',
                                                                                       FinServ__Ownership__c = 'Individual',
                                                                                       FinServ__PrimaryOwner__c = accObj3.Id,
                                                                                       FinServ__Status__c = 'Open',
                                                                                       FinServ__OpenDate__c = System.today()-40,
                                                                                       RecordTypeId = preRecTypeId,
                                                                                       OwnerId = userObj4.Id);
            finAccList.add(finAccObj3);
            
            FinServ__FinancialAccount__c finAccObj4 = new FinServ__FinancialAccount__c(Name = 'Premier Account 4',
                                                                                       FinServ__Ownership__c = 'Individual',
                                                                                       FinServ__PrimaryOwner__c = accObj3.Id,
                                                                                       FinServ__Status__c = 'Pending',
                                                                                       RecordTypeId = preRecTypeId,
                                                                                       OwnerId = userObj5.Id);
            finAccList.add(finAccObj4);
            
            FinServ__FinancialAccount__c finAccObj5 = new FinServ__FinancialAccount__c(Name = 'CIS Account 1',
                                                                                       FinServ__Ownership__c = 'Individual',
                                                                                       FinServ__PrimaryOwner__c = accObj2.Id,
                                                                                       FinServ__Status__c = 'Open',
                                                                                       FinServ__OpenDate__c = System.today()-30,
                                                                                       RecordTypeId = cisRecTypeId,
                                                                                       OwnerId = userObj1.Id);
            finAccList.add(finAccObj5);
            
            FinServ__FinancialAccount__c finAccObj6 = new FinServ__FinancialAccount__c(Name = 'CIS Account 2',
                                                                                       FinServ__Ownership__c = 'Individual',
                                                                                       FinServ__PrimaryOwner__c = accObj2.Id,
                                                                                       FinServ__Status__c = 'Open',
                                                                                       FinServ__OpenDate__c = System.today()-10,
                                                                                       RecordTypeId = cisRecTypeId,
                                                                                       OwnerId = userObj2.Id);
            finAccList.add(finAccObj6);
            
            FinServ__FinancialAccount__c finAccObj7 = new FinServ__FinancialAccount__c(Name = 'CIS Account 3',
                                                                                       FinServ__Ownership__c = 'Individual',
                                                                                       FinServ__PrimaryOwner__c = accObj2.Id,
                                                                                       FinServ__Status__c = 'Open',
                                                                                       FinServ__OpenDate__c = System.today()-5,
                                                                                       RecordTypeId = cisRecTypeId,
                                                                                       OwnerId = userObj3.Id);
            finAccList.add(finAccObj7);
            
            FinServ__FinancialAccount__c finAccObj8 = new FinServ__FinancialAccount__c(Name = 'CIS Account 4',
                                                                                       FinServ__Ownership__c = 'Individual',
                                                                                       FinServ__PrimaryOwner__c = accObj1.Id,
                                                                                       FinServ__Status__c = 'Open',
                                                                                       FinServ__OpenDate__c = System.today()-30,
                                                                                       RecordTypeId = cisRecTypeId,
                                                                                       OwnerId = userObj1.Id);
            finAccList.add(finAccObj8);
            
            FinServ__FinancialAccount__c finAccObj9 = new FinServ__FinancialAccount__c(Name = 'CIS Account 5',
                                                                                       FinServ__Ownership__c = 'Individual',
                                                                                       FinServ__PrimaryOwner__c = accObj1.Id,
                                                                                       FinServ__Status__c = 'On Hold',
                                                                                       RecordTypeId = cisRecTypeId,
                                                                                       OwnerId = userObj2.Id);
            finAccList.add(finAccObj9);
            
            FinServ__FinancialAccount__c finAccObj10 = new FinServ__FinancialAccount__c(Name = 'CIS Account 6',
                                                                                        FinServ__Ownership__c = 'Individual',
                                                                                        FinServ__PrimaryOwner__c = accObj3.Id,
                                                                                        FinServ__Status__c = 'Open',
                                                                                        FinServ__OpenDate__c = System.today()-1,
                                                                                        RecordTypeId = cisRecTypeId,
                                                                                        OwnerId = userObj3.Id);
            finAccList.add(finAccObj10);
            
            /* V1.1 Starts */
            
            FinServ__FinancialAccount__c finAccObj15 = new FinServ__FinancialAccount__c(Name = 'CIS Account 7',
                                                                                        FinServ__Ownership__c = 'Individual',
                                                                                        FinServ__PrimaryOwner__c = accObj4.Id,
                                                                                        FinServ__Status__c = 'Open',
                                                                                        FinServ__OpenDate__c = System.today()-50,
                                                                                        RecordTypeId = cisRecTypeId,
                                                                                        OwnerId = userObj3.Id);
            finAccList.add(finAccObj15);
            
            FinServ__FinancialAccount__c finAccObj16 = new FinServ__FinancialAccount__c(Name = 'Premier Account 5',
                                                                                        FinServ__Ownership__c = 'Individual',
                                                                                        FinServ__PrimaryOwner__c = accObj4.Id,
                                                                                        FinServ__Status__c = 'Open',
                                                                                        FinServ__OpenDate__c = System.today()-1,
                                                                                        RecordTypeId = preRecTypeId,
                                                                                        OwnerId = userObj5.Id);
            finAccList.add(finAccObj16);
            
            FinServ__FinancialAccount__c finAccObj17 = new FinServ__FinancialAccount__c(Name = 'Premier Account 6',
                                                                                        FinServ__Ownership__c = 'Individual',
                                                                                        FinServ__PrimaryOwner__c = accObj4.Id,
                                                                                        FinServ__Status__c = 'Open',
                                                                                        FinServ__OpenDate__c = System.today()-10,
                                                                                        RecordTypeId = preRecTypeId,
                                                                                        OwnerId = userObj4.Id);
            finAccList.add(finAccObj17);
            
            /* V1.1 Ends */
            insert finAccList;
            Test.startTest();
            SkienceEx_BinNumberScheduler.scheduleMe();
            Test.stopTest();
        }
    }
    
}